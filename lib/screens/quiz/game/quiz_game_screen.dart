import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mlearning/utils/constanta_colors.dart';
import 'package:mlearning/utils/size_config.dart';
import 'package:mlearning/data/model/quiz_model.dart';

class QuizGameScreen extends StatefulWidget {
  final QuizModel quizModel;

  QuizGameScreen({this.quizModel}) : super();

  @override
  _QuizGameState createState() => new _QuizGameState(quizModel: quizModel);
}

class _QuizGameState extends State<QuizGameScreen> {
  final QuizModel quizModel;
  _QuizGameState({this.quizModel});

  List<int> value = [2, 2, 2, 2, 2, 2, 2, 2, 2, 2];
  String selectedChoice1;
  String selectedChoice2;
  String selectedChoice3;
  String selectedChoice4;
  String selectedChoice5;
  String selectedChoice6;
  String selectedChoice7;
  String selectedChoice8;
  String selectedChoice9;
  String selectedChoice10;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(quizModel.title),
      ),
      body: Stack(
        children: [
          Column(
            children: [
              Spacer(),
              SvgPicture.asset(
                "assets/illustrations/quiz.svg",
                width: getProportionateScreenWidth(414.0),
              )
            ],
          ),
          Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: getProportionateScreenWidth(24.0)),
              child: _buildListView(context, quizModel)
          )
        ],
      ),
    );
  }

  Widget _buildListView(BuildContext context, QuizModel data) {
    final quizItem = data.quiz_item;

    if (quizItem.length == 10){
      return SingleChildScrollView(
        child: Stack(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(quizItem[0].question),
                Column(
                    children: createRadioQuiz1(quizItem[0], 0)
                ),
                Text(quizItem[1].question),
                Column(
                    children: createRadioQuiz2(quizItem[1], 1)
                ),
                Text(quizItem[2].question),
                Column(
                    children: createRadioQuiz3(quizItem[2], 2)
                ),
                Text(quizItem[3].question),
                Column(
                    children: createRadioQuiz4(quizItem[3], 3)
                ),
                Text(quizItem[4].question),
                Column(
                    children: createRadioQuiz5(quizItem[4], 4)
                ),
                Text(quizItem[5].question),
                Column(
                    children: createRadioQuiz6(quizItem[5], 5)
                ),
                Text(quizItem[6].question),
                Column(
                    children: createRadioQuiz7(quizItem[6], 6)
                ),
                Text(quizItem[7].question),
                Column(
                    children: createRadioQuiz8(quizItem[7], 7)
                ),
                Text(quizItem[8].question),
                Column(
                    children: createRadioQuiz9(quizItem[8], 8)
                ),
                Text(quizItem[9].question),
                Column(
                    children: createRadioQuiz10(quizItem[9], 9)
                ),
              ],
            )
          ],
        ),
      );
    } else {
      return Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text("Maaf data pertanyaan belum mencapai 10")
          ]
      );
    }

  }

  List<Widget> createRadioQuiz1(QuizItemModel quizItemModel, int index) {
    var choices = quizItemModel.choice.split(',').toList();
    var answer = quizItemModel.answer;
    List<Widget> widgets = [];
    for (String choice in choices) {
      widgets.add(
        RadioListTile(
          value: choice,
          groupValue: selectedChoice1,
          title: Text(choice),
          onChanged: (val) {
            setState(() {
              selectedChoice1 = choice;
              if (choice == answer){
                value[index] = 1;
              } else {
                value[index] = 2;
              }
              print(value);
            });
          },
          selected: selectedChoice1 == choice,
          activeColor: Colors.orange,
        ),
      );
    }
    return widgets;
  }

  List<Widget> createRadioQuiz2(QuizItemModel quizItemModel, int index) {
    var choices = quizItemModel.choice.split(',').toList();
    var answer = quizItemModel.answer;
    List<Widget> widgets = [];
    for (String choice in choices) {
      widgets.add(
        RadioListTile(
          value: choice,
          groupValue: selectedChoice2,
          title: Text(choice),
          onChanged: (val) {
            setState(() {
              selectedChoice2 = choice;
              if (choice == answer){
                value[index] = 1;
              } else {
                value[index] = 2;
              }
              print(value);
            });
          },
          selected: selectedChoice2 == choice,
          activeColor: Colors.orange,
        ),
      );
    }
    return widgets;
  }

  List<Widget> createRadioQuiz3(QuizItemModel quizItemModel, int index) {
    var choices = quizItemModel.choice.split(',').toList();
    var answer = quizItemModel.answer;
    List<Widget> widgets = [];
    for (String choice in choices) {
      widgets.add(
        RadioListTile(
          value: choice,
          groupValue: selectedChoice3,
          title: Text(choice),
          onChanged: (val) {
            setState(() {
              selectedChoice3 = choice;
              if (choice == answer){
                value[index] = 1;
              } else {
                value[index] = 2;
              }
              print(value);
            });
          },
          selected: selectedChoice3 == choice,
          activeColor: Colors.orange,
        ),
      );
    }
    return widgets;
  }

  List<Widget> createRadioQuiz4(QuizItemModel quizItemModel, int index) {
    var choices = quizItemModel.choice.split(',').toList();
    var answer = quizItemModel.answer;
    List<Widget> widgets = [];
    for (String choice in choices) {
      widgets.add(
        RadioListTile(
          value: choice,
          groupValue: selectedChoice4,
          title: Text(choice),
          onChanged: (val) {
            setState(() {
              selectedChoice4 = choice;
              if (choice == answer){
                value[index] = 1;
              } else {
                value[index] = 2;
              }
              print(value);
            });
          },
          selected: selectedChoice4 == choice,
          activeColor: Colors.orange,
        ),
      );
    }
    return widgets;
  }

  List<Widget> createRadioQuiz5(QuizItemModel quizItemModel, int index) {
    var choices = quizItemModel.choice.split(',').toList();
    var answer = quizItemModel.answer;
    List<Widget> widgets = [];
    for (String choice in choices) {
      widgets.add(
        RadioListTile(
          value: choice,
          groupValue: selectedChoice5,
          title: Text(choice),
          onChanged: (val) {
            setState(() {
              selectedChoice5 = choice;
              if (choice == answer){
                value[index] = 1;
              } else {
                value[index] = 2;
              }
              print(value);
            });
          },
          selected: selectedChoice5 == choice,
          activeColor: Colors.orange,
        ),
      );
    }
    return widgets;
  }

  List<Widget> createRadioQuiz6(QuizItemModel quizItemModel, int index) {
    var choices = quizItemModel.choice.split(',').toList();
    var answer = quizItemModel.answer;
    List<Widget> widgets = [];
    for (String choice in choices) {
      widgets.add(
        RadioListTile(
          value: choice,
          groupValue: selectedChoice6,
          title: Text(choice),
          onChanged: (val) {
            setState(() {
              selectedChoice6 = choice;
              if (choice == answer){
                value[index] = 1;
              } else {
                value[index] = 2;
              }
              print(value);
            });
          },
          selected: selectedChoice6 == choice,
          activeColor: Colors.orange,
        ),
      );
    }
    return widgets;
  }

  List<Widget> createRadioQuiz7(QuizItemModel quizItemModel, int index) {
    var choices = quizItemModel.choice.split(',').toList();
    var answer = quizItemModel.answer;
    List<Widget> widgets = [];
    for (String choice in choices) {
      widgets.add(
        RadioListTile(
          value: choice,
          groupValue: selectedChoice7,
          title: Text(choice),
          onChanged: (val) {
            setState(() {
              selectedChoice7 = choice;
              if (choice == answer){
                value[index] = 1;
              } else {
                value[index] = 2;
              }
              print(value);
            });
          },
          selected: selectedChoice7 == choice,
          activeColor: Colors.orange,
        ),
      );
    }
    return widgets;
  }

  List<Widget> createRadioQuiz8(QuizItemModel quizItemModel, int index) {
    var choices = quizItemModel.choice.split(',').toList();
    var answer = quizItemModel.answer;
    List<Widget> widgets = [];
    for (String choice in choices) {
      widgets.add(
        RadioListTile(
          value: choice,
          groupValue: selectedChoice8,
          title: Text(choice),
          onChanged: (val) {
            setState(() {
              selectedChoice8 = choice;
              if (choice == answer){
                value[index] = 1;
              } else {
                value[index] = 2;
              }
              print(value);
            });
          },
          selected: selectedChoice8 == choice,
          activeColor: Colors.orange,
        ),
      );
    }
    return widgets;
  }

  List<Widget> createRadioQuiz9(QuizItemModel quizItemModel, int index) {
    var choices = quizItemModel.choice.split(',').toList();
    var answer = quizItemModel.answer;
    List<Widget> widgets = [];
    for (String choice in choices) {
      widgets.add(
        RadioListTile(
          value: choice,
          groupValue: selectedChoice9,
          title: Text(choice),
          onChanged: (val) {
            setState(() {
              selectedChoice9 = choice;
              if (choice == answer){
                value[index] = 1;
              } else {
                value[index] = 2;
              }
              print(value);
            });
          },
          selected: selectedChoice9 == choice,
          activeColor: Colors.orange,
        ),
      );
    }
    return widgets;
  }

  List<Widget> createRadioQuiz10(QuizItemModel quizItemModel, int index) {
    var choices = quizItemModel.choice.split(',').toList();
    var answer = quizItemModel.answer;
    List<Widget> widgets = [];
    for (String choice in choices) {
      widgets.add(
        RadioListTile(
          value: choice,
          groupValue: selectedChoice10,
          title: Text(choice),
          onChanged: (val) {
            setState(() {
              selectedChoice10 = choice;
              if (choice == answer){
                value[index] = 1;
              } else {
                value[index] = 2;
              }
              print(value);
            });
          },
          selected: selectedChoice10 == choice,
          activeColor: Colors.orange,
        ),
      );
    }
    return widgets;
  }

}
