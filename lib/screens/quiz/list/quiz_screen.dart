import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mlearning/routing/constanta_routing.dart';
import 'package:mlearning/screens/quiz/game/quiz_game_screen.dart';
import 'package:mlearning/utils/constanta_colors.dart';
import 'package:mlearning/utils/size_config.dart';
import 'package:mlearning/data/model/quiz_model.dart';
import 'package:mlearning/data/bloc/quiz_bloc.dart';

class QuizScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    blocQuiz.getQuizAll();
    return Scaffold(
      appBar: AppBar(
        title: Text("Kuis"),
      ),
      body: Stack(
        children: [
          Column(
            children: [
              Spacer(),
              SvgPicture.asset(
                "assets/illustrations/quiz.svg",
                width: getProportionateScreenWidth(414.0),
              )
            ],
          ),
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: getProportionateScreenWidth(24.0)),
            child: StreamBuilder<QuizResponse>(
                stream: blocQuiz.subject.stream,
                builder: (context, AsyncSnapshot<QuizResponse> snapshot){
                  if (snapshot.hasData) {
                    if (snapshot.data.error.length > 0){
                      return _buildErrorWidget(snapshot.data.error);
                    }

                    return _buildListView(context, snapshot.data);
                  } else if (snapshot.hasError){
                    return _buildErrorWidget(snapshot.error.toString());
                  } else {
                    return _buildLoadingWidget();
                  }
                }
            )
          )
        ],
      ),
    );
  }

  Widget _buildLoadingWidget() {
    return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [Text("Loading data from API..."), CircularProgressIndicator()],
        ));
  }

  Widget _buildErrorWidget(String error) {
    return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text("Error occured: $error"),
          ],
        ));
  }

  Widget _buildListView(BuildContext context, QuizResponse data) {
    return
      ListView.builder(itemBuilder: (context,index){
        return GestureDetector(
          onTap: () => {
            Navigator.pushReplacement(context, MaterialPageRoute(builder: (_) => QuizGameScreen(quizModel: data.records[index],)))
          },
          child: Container(
            width: double.infinity,
            height: getProportionateScreenHeight(62.0),
            margin: EdgeInsets.only(
                bottom: getProportionateScreenWidth(16.0)),
            decoration: BoxDecoration(
                color: kBackgroundCard,
                borderRadius: BorderRadius.circular(
                    getProportionateScreenWidth(8.0))),
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: getProportionateScreenWidth(24.0)),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  data.records[index].title,
                  style: TextStyle(color: kText1, fontSize: 16.0),
                ),
              ),
            ),
          ),
        );
      },itemCount: data.records.length,
      );
  }
}
