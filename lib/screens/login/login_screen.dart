import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mlearning/routing/constanta_routing.dart';
import 'package:mlearning/utils/constanta_colors.dart';
import 'package:mlearning/utils/size_config.dart';
import 'package:mlearning/data/model/user_model.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mlearning/data/remote/api_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';


class LoginScreen extends StatefulWidget {
  @override
  _LoginState createState() => new _LoginState();
}

class _LoginState extends State<LoginScreen>{
  SharedPreferences sharedPreferences;
  ApiProvider _provider = ApiProvider();
  final _key = new GlobalKey<FormState>();
  String noInduk;
  bool checkValue = false;

  @override
  void initState() {
    super.initState();
    getCredential();
  }

  getCredential() async {
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      checkValue = sharedPreferences.getBool("check");
      if (checkValue != null) {
        if (checkValue) {
          navigate(context);
        } else {
          sharedPreferences.clear();
        }
      } else {
        checkValue = false;
      }
    });
  }

  login(BuildContext context) async {
    UserResponse response = await _provider.getUserLogin(noInduk);

    if (response.error.isEmpty){
      final data = response.records;

      if (data.isNotEmpty){
        sharedPreferences = await SharedPreferences.getInstance();
        setState(() {
          sharedPreferences.setBool("check", true);
          sharedPreferences.setInt("id_user", data[0].id_user);
          sharedPreferences.setInt("no_induk", data[0].no_induk);
          sharedPreferences.setString("nama", data[0].nama);
        });
        Navigator.pushReplacementNamed(context, onBoarding);
      } else {
        showToast("NISN/NISP anda salah !!!");
      }
    } else {
      showToast(response.error);
    }
  }

  check(BuildContext context) {
    final form = _key.currentState;
    if (form.validate()) {
      form.save();
      login(context);
    }
  }

  navigate(BuildContext context){
    Navigator.pushReplacementNamed(context, onBoarding);
  }

  showToast(String toast) async {
    Fluttertoast.showToast(
        msg: toast,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: kPrimary,
        textColor: Colors.white,
        fontSize: 16.0
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(getProportionateScreenWidth(24.0)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: getProportionateScreenHeight(24.0),
              ),
              Align(
                  alignment: Alignment.center,
                  child: SvgPicture.asset("assets/illustrations/login.svg")),
              SizedBox(
                height: getProportionateScreenHeight(56.0),
              ),
              Text(
                "NISN / NISP",
                style: TextStyle(color: kText2, fontSize: 16.0),
              ),
              SizedBox(
                height: getProportionateScreenHeight(4.0),
              ),
              Form(
                  key: _key,
                  child: TextFormField(
                      validator: (e){
                        if (e.isEmpty){
                          return "Tolong NISN/NISP diisi";
                        }
                      },
                      onSaved: (e) => noInduk = e,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(
                              horizontal: getProportionateScreenWidth(24.0)),
                          filled: true,
                          hintText: "Masukan NISN/NISP Anda",
                          hintStyle: TextStyle(color: kText2, fontSize: 14.0),
                          fillColor: kBackgroundCard,
                          border: InputBorder.none,
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(100.0),
                              borderSide: BorderSide(style: BorderStyle.none)),
                          focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(100.0),
                              borderSide:
                              BorderSide(style: BorderStyle.none))))),
              SizedBox(
                height: getProportionateScreenHeight(24.0),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: ConstrainedBox(
                  constraints: BoxConstraints.tightFor(width: double.infinity),
                  child: ElevatedButton(
                      onPressed: () {
                        check(context);
                      },
                      style: ButtonStyle(
                          padding: MaterialStateProperty.all(EdgeInsets.all(
                              getProportionateScreenWidth(16.0))),
                          elevation: MaterialStateProperty.all(0),
                          shape: MaterialStateProperty.all(
                              RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(100.0),
                                      topRight: Radius.circular(8.0),
                                      bottomRight: Radius.circular(100.0),
                                      bottomLeft: Radius.circular(100.0)))),
                          backgroundColor: MaterialStateProperty.all(kPrimary)),
                      child: Text(
                        "Masuk",
                        style: TextStyle(color: kWhite, fontSize: 16.0),
                      )),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
