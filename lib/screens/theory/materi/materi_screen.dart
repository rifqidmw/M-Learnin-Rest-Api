import 'dart:io';
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:mlearning/utils/constanta_colors.dart';
import 'package:mlearning/utils/size_config.dart';
import 'package:advance_pdf_viewer/advance_pdf_viewer.dart';
import 'package:mlearning/data/model/materi_model.dart';

class MateriApp extends StatefulWidget {
  final MateriModel materiModel;

  MateriApp({this.materiModel}): super();
  @override
  _MateriAppState createState() => new _MateriAppState(materiModel: materiModel);
}

class _MateriAppState extends State<MateriApp> {
  final MateriModel materiModel;
  _MateriAppState({this.materiModel});
  bool _isLoading = true;
  PDFDocument document;

  @override
  void initState() {
    super.initState();

    loadDocument();
  }

  loadDocument() async {
    document = await PDFDocument.fromURL(materiModel.url);

    setState(() => _isLoading = false);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: Text(materiModel.title),
          ),
        body: Center(
          child: _isLoading
                ? Center(child: CircularProgressIndicator())
                : PDFViewer(
              document: document,
              zoomSteps: 1,
              scrollDirection: Axis.vertical,
          )
        )
      ),
    );
  }
}

