import 'package:mlearning/data/model/materi_model.dart';
import 'package:mlearning/data/model/quiz_model.dart';
import 'package:mlearning/data/model/user_model.dart';
import 'package:mlearning/utils/logging_interceptor.dart';
import 'package:dio/dio.dart';

class ApiProvider {
  final String _endpoint = "http://192.168.1.6/m_learning/api.php/records/";
  Dio _dio = Dio();

  ApiProvider(){
    BaseOptions options =
    BaseOptions(receiveTimeout: 5000, connectTimeout: 5000);
    _dio = Dio(options);
    _dio.interceptors.add(LoggingInterceptor());
  }

  //User
  Future<UserResponse> getUserLogin(String noInduk) async {
    try {
      Response response = await _dio.get("${_endpoint}user?filter=no_induk,eq,$noInduk");
      print(response.data.toString());
      return UserResponse.fromJson(response.data);
    } on Error catch (error, stacktrace) {
      print("Exception occured: $error stacTrace: $stacktrace");
      return UserResponse.witError(_handleError(error));
    }
  }

  //Materi
  Future<MateriResponse> getMateriAll() async {
    try {
      Response response = await _dio.get("${_endpoint}materi?join=mapel");
      print(response.data.toString());
      return MateriResponse.fromJson(response.data);
    } on Error catch (error, stacktrace) {
      print("Exception occured: $error stacTrace: $stacktrace");
      return MateriResponse.witError(_handleError(error));
    }
  }

  //Quiz
  Future<QuizResponse> getQuizAll() async {
    try {
      Response response = await _dio.get("${_endpoint}quiz?join=quiz_item,quiz_choice&join=mapel");
      print(response.data.toString());
      return QuizResponse.fromJson(response.data);
    } on Error catch (error, stacktrace){
      print("Exception occured: $error stacTrace: $stacktrace");
      return QuizResponse.witError(_handleError(error));
    }
  }

  Future<QuizResponse> getQuiz(int id) async {
    try {
      Response response = await _dio.get("${_endpoint}quiz?filter=id_quiz,eq,$id&join=quiz_item,quiz_choice&join=mapel");
      print(response.data.toString());
      return QuizResponse.fromJson(response.data);
    } on Error catch (error, stacktrace){
      print("Exception occured: $error stacTrace: $stacktrace");
      return QuizResponse.witError(_handleError(error));
    }
  }

  String _handleError(Error error) {
    String errorDescription = "";
    if (error is DioError) {
      DioError dioError = error as DioError;
      switch (dioError.type) {
        case DioErrorType.CANCEL:
          errorDescription = "Request to API server was cancelled";
          break;
        case DioErrorType.CONNECT_TIMEOUT:
          errorDescription = "Connection timeout with API server";
          break;
        case DioErrorType.DEFAULT:
          errorDescription =
          "Connection to API server failed due to internet connection";
          break;
        case DioErrorType.RECEIVE_TIMEOUT:
          errorDescription = "Receive timeout in connection with API server";
          break;
        case DioErrorType.RESPONSE:
          errorDescription =
          "Received invalid status code: ${dioError.response.statusCode}";
          break;
        case DioErrorType.SEND_TIMEOUT:
          errorDescription = "Send timeout in connection with API server";
          break;
      }
    } else {
      errorDescription = "Unexpected error occured";
    }
    return errorDescription;
  }
}