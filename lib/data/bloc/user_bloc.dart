import 'package:mlearning/data/model/quiz_model.dart';
import 'package:mlearning/data/model/user_model.dart';
import 'package:mlearning/data/repository/quiz_repository.dart';
import 'package:mlearning/data/repository/user_repository.dart';
import 'package:rxdart/rxdart.dart';

class UserBloc {
  final UserRepository _repository = UserRepository();
  final BehaviorSubject<UserResponse> _subject = BehaviorSubject<UserResponse>();

  loginUser(String noInduk) async {
    UserResponse response = await _repository.userLogin(noInduk);
    _subject.sink.add(response);
  }

  dispose() {
    _subject.close();
  }

  BehaviorSubject<UserResponse> get subject => _subject;
}
final blocUser = UserBloc();