import 'package:mlearning/data/model/quiz_model.dart';
import 'package:mlearning/data/repository/quiz_repository.dart';
import 'package:rxdart/rxdart.dart';

class QuizBloc {
  final QuizRepository _repository = QuizRepository();
  final BehaviorSubject<QuizResponse> _subject = BehaviorSubject<QuizResponse>();

  getQuizAll() async {
    QuizResponse response = await _repository.getQuizAll();
    _subject.sink.add(response);
  }

  getQuiz(int id) async {
    QuizResponse response = await _repository.getQuiz(id);
    _subject.sink.add(response);
  }

  dispose() {
    _subject.close();
  }

  BehaviorSubject<QuizResponse> get subject => _subject;
}
final blocQuiz = QuizBloc();