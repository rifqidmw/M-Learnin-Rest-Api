import 'package:mlearning/data/model/materi_model.dart';
import 'package:mlearning/data/repository/materi_repository.dart';
import 'package:rxdart/rxdart.dart';

class MateriBloc {
  final MateriRepository _repository = MateriRepository();
  final BehaviorSubject<MateriResponse> _subject = BehaviorSubject<MateriResponse>();

  getMateriAll() async {
    MateriResponse response = await _repository.getMateriAll();
    _subject.sink.add(response);
  }

  dispose() {
    _subject.close();
  }

  BehaviorSubject<MateriResponse> get subject => _subject;
}
final blocMateri = MateriBloc();