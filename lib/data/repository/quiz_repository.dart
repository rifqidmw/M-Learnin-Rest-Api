import 'package:mlearning/data/model/materi_model.dart';
import 'package:mlearning/data/model/quiz_model.dart';
import 'package:mlearning/data/remote/api_provider.dart';

class QuizRepository {
  ApiProvider _apiProvider = ApiProvider();

  Future<QuizResponse> getQuizAll(){
    return _apiProvider.getQuizAll();
  }

  Future<QuizResponse> getQuiz(int id){
    return _apiProvider.getQuiz(id);
  }
}