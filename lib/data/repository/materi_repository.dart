import 'package:mlearning/data/model/materi_model.dart';
import 'package:mlearning/data/remote/api_provider.dart';

class MateriRepository {
  ApiProvider _apiProvider = ApiProvider();

  Future<MateriResponse> getMateriAll(){
    return _apiProvider.getMateriAll();
  }
}