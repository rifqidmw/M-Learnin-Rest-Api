import 'package:mlearning/data/model/materi_model.dart';
import 'package:mlearning/data/model/quiz_model.dart';
import 'package:mlearning/data/model/user_model.dart';
import 'package:mlearning/data/remote/api_provider.dart';

class UserRepository {
  ApiProvider _apiProvider = ApiProvider();

  Future<UserResponse> userLogin(String noInduk){
    return _apiProvider.getUserLogin(noInduk);
  }
}