import 'dart:convert';

class MateriResponse {
  final List<MateriModel> records;
  final String error;

  MateriResponse(this.records, this.error);

  MateriResponse.fromJson(Map<String, dynamic> json) :
      records = (json['records'] as List).map((i) => new MateriModel.fromJson(i)).toList(), error = "";

  MateriResponse.witError(String errorValue) :
      records = List.empty(),
      error = errorValue;
}

class MateriModel {
  final int id_materi;
  final String title;
  final String url;
  final DataMapel mapel;

  const MateriModel({
    this.id_materi,
    this.title,
    this.url,
    this.mapel
  });

  MateriModel.fromJson(Map<String, dynamic> json) :
      id_materi = json['id_materi'],
      title = json['title'],
      url = json['url'],
      mapel = DataMapel.fromJson(json['mapel']);
}

class DataMapel {
  final int id_mapel;
  final String name;

  const DataMapel({
    this.id_mapel,
    this.name
  });

  
  DataMapel.fromJson(Map<String, dynamic> json)
      : id_mapel = json["id_mapel"],
        name = json["name"];

}