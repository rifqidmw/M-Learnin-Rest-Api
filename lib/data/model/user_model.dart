import 'dart:convert';

class UserResponse {
  final List<UserModel> records;
  final String error;

  UserResponse(this.records, this.error);

  UserResponse.fromJson(Map<String, dynamic> json) :
      records = (json['records'] as List).map((i) => new UserModel.fromJson(i)).toList(), error = "";

  UserResponse.witError(String errorValue) :
      records = List.empty(),
      error = errorValue;
}

class UserModel {
  final int id_user;
  final int no_induk;
  final String nama;
  final String role;

  const UserModel({
    this.id_user,
    this.no_induk,
    this.nama,
    this.role
  });

  UserModel.fromJson(Map<String, dynamic> json) :
        id_user = json['id_user'],
      no_induk = json['no_induk'],
      nama = json['nama'],
      role = json['role'];
}