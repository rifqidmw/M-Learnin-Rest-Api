import 'dart:convert';

class QuizResponse {
  final List<QuizModel> records;
  final String error;

  QuizResponse(this.records, this.error);

  QuizResponse.fromJson(Map<String, dynamic> json) :
      records = (json['records'] as List).map((i) => new QuizModel.fromJson(i)).toList(), error = "";

  QuizResponse.witError(String errorValue) :
      records = List.empty(),
      error = errorValue;
}

class QuizModel {
  final int id_quiz;
  final String title;
  final MapelModel mapel;
  final List<QuizItemModel> quiz_item;

  const QuizModel({
    this.id_quiz,
    this.title,
    this.mapel,
    this.quiz_item
  });

  QuizModel.fromJson(Map<String, dynamic> json) :
        id_quiz = json['id_quiz'],
      title = json['title'],
      mapel = MapelModel.fromJson(json['mapel']),
      quiz_item = (json['quiz_item'] as List).map((i) => QuizItemModel.fromJson(i)).toList();
}

class QuizItemModel{
  final int id_quiz_item;
  final String question;
  final String choice;
  final String answer;
  final List<QuizChoiceModel> quiz_choice;
  
  const QuizItemModel({
    this.id_quiz_item,
    this.question,
    this.choice,
    this.answer,
    this.quiz_choice
  });
  
  QuizItemModel.fromJson(Map<String, dynamic> json) :
      id_quiz_item = json['id_quiz_item'],
      question = json['question'],
      choice = json['choice'],
      answer = json['answer'],
      quiz_choice = (json['quiz_choice'] as List).map((i) => QuizChoiceModel.fromJson(i)).toList();
}

class QuizChoiceModel {
  final int id_quiz_choice;
  final String choice;
  final int bobot;

  const QuizChoiceModel({
    this.id_quiz_choice,
    this.choice,
    this.bobot
  });

  QuizChoiceModel.fromJson(Map<String, dynamic> json) :
      id_quiz_choice = json['id_quiz_choice'],
      choice = json['choice'],
      bobot = json ['bobot'];
}

class MapelModel {
  final int id_mapel;
  final String name;

  const MapelModel({
    this.id_mapel,
    this.name
  });


  MapelModel.fromJson(Map<String, dynamic> json)
      : id_mapel = json["id_mapel"],
        name = json["name"];

}