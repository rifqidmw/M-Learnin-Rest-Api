const splash = '/';
const onBoarding = '/onBoarding';
const login = "/login";
const home = "/home";

const theory = "/theory";
const materi = "/materi";
const video = "/video";
const quiz = "/quiz";
const quizGame = "/quizGame";
const qna = "/qna";

const about = "/about";
