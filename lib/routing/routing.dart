import 'package:flutter/material.dart';
import 'package:mlearning/routing/constanta_routing.dart';
import 'package:mlearning/screens/home/home_screen.dart';
import 'package:mlearning/screens/login/login_screen.dart';
import 'package:mlearning/screens/onboarding/onboarding_screen.dart';
import 'package:mlearning/screens/qna/qna_screen.dart';
import 'package:mlearning/screens/quiz/game/quiz_game_screen.dart';
import 'file:///D:/Project/Flutter/mlearningui/lib/screens/quiz/list/quiz_screen.dart';
import 'package:mlearning/screens/splash/splash_screen.dart';
import 'package:mlearning/screens/theory/materi/materi_screen.dart';
import 'file:///D:/Project/Flutter/mlearningui/lib/screens/theory/list/theory_screen.dart';
import 'package:mlearning/screens/video/video_screen.dart';

class Routes {
  static Route<dynamic> generateRoute(RouteSettings routeSettings) {
    switch (routeSettings.name) {
      case onBoarding:
        return MaterialPageRoute(builder: (_) => OnBoardingScreen());
      case login:
        return MaterialPageRoute(builder: (_) => LoginScreen());
      case home:
        return MaterialPageRoute(builder: (_) => HomeScreen());
      case theory:
        return MaterialPageRoute(builder: (_) => TheoryScreen());
      case video:
        return MaterialPageRoute(builder: (_) => VideoScreen());
      case quiz:
        return MaterialPageRoute(builder: (_) => QuizScreen());
      case quizGame:
        return MaterialPageRoute(builder: (_) => QuizGameScreen());
      case qna:
        return MaterialPageRoute(builder: (_) => QnaScreen());
      default:
        return MaterialPageRoute(builder: (_) => SplashScreen());
    }
  }
}
